#ifndef _GLOBAL_H_
#define _GLOBAL_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#define MAXLINESIZE 50000

typedef unsigned char byte; //!<  8 bit unsigned
typedef int int32;

typedef struct {
  int width;     // image width
  int height;    // image height
  int width_uv;  // image width
  int height_uv; // image height

  char infile[100]; // name of input file
  char outfile[100];
  int frame_no; // number of current frame that is processed
  unsigned int RB[3];
  int CodeRange[3];
} ImageParameters;

typedef struct {
  int no_frames;        // number of frames to process
  int width;            // image width
  int height;           // image height
  char infile_pre[100]; // input prefix
  char infile_suf[100]; // input suffix
  int infile_first;     // number of first frame to process
  int infile_count;     // number of digits for frame count
  char outfile[100];    // name of output file
  int subsampling;      // color subsampling
  int yuv_format;       // Abekas or Planar yuv
  int bpp_out;  // Output may be 8,or 10 bit, always with headroom/footroom
  int stuffing; // If set to one, 10 bit will be written into 2 bytes with
                // stuffing '0' bits
} InputParameters;

#define ET_SIZE 300 //!< size of error text buffer
extern char
    errortext[ET_SIZE]; //!< buffer for error message for exit with error()

extern InputParameters *input;
extern ImageParameters *img;

// Prototypes
int main();
void Configure(int ac, char *av[]);
void rgb2yuv();
void malloc_memory();
void read_frame();

void write_image_yuv8bit();
void write_image_abekas8bit();
void write_image_yuv10bit();
void write_image_abekas10bit();
void write_image_yuv10bit_st();
void write_image_abekas10bit_st();

void free_memory();
void error(char *text, int code);
int Value2Code(double f, int max, int min, int range, int zero);
int get_mem2D(int ***array2D, int rows, int columns);
int get_mem2D_double(double ***array2D, int rows, int columns);
void no_mem_exit(char *where);
void free_mem2D(int **array2D);
void free_mem2D_double(double **array2D);
void error(char *text, int code);

void subsampling();
void Hsubsampling(double **full, double **sub, int Hfactor, int filterh);
void Vsubsampling(double **full, double **sub, int Vfactor, int filterv);

#endif // _GLOBAL_H_
