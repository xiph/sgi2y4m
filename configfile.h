/*!
 ***********************************************************************
 *  \file
 *     configfile.h
 *  \brief
 *     Prototypes for configfile.c and definitions of used structures.
 ***********************************************************************
 */

#ifndef _CONFIGFILE_H_
#define _CONFIGFILE_H_

#define DEFAULTCONFIGFILENAME "sgi2yuv.cfg"

typedef struct {
  char *TokenName;
  void *Place;
  int Type;
} Mapping;

#ifdef INCLUDED_BY_CONFIGFILE_C

#include "global.h" // defines InputParamaters

InputParameters configinput;

Mapping Map[] = {
  {"FramesToBeConverted", &configinput.no_frames,    0},
  {"SourceWidth",         &configinput.width,        0},
  {"SourceHeight",        &configinput.height,       0},
  {"InputFilePrefix",     &configinput.infile_pre,   1},
  {"InputFileSuffix",     &configinput.infile_suf,   1},
  {"InputFileFirstFrame", &configinput.infile_first, 0},
  {"Count_digits",        &configinput.infile_count, 0},
  {"OutputFile",          &configinput.outfile,      1},
  {"Color_subsampling",   &configinput.subsampling,  0},
  {"YUV_Format",          &configinput.yuv_format,   0},
  {"BitDepth_out",        &configinput.bpp_out,      0},
  {"Stuffing16bit",       &configinput.stuffing,     0},
  {NULL,                  NULL,                     -1}};

#endif

#ifndef INCLUDED_BY_CONFIGFILE_C
extern Mapping Map[];
#endif

void Configure(int ac, char *av[]);

#endif
