CFLAGS=-Wall -D_LARGEFILE64_SOURCE -D_FILE_OFFSET_BITS=64
CFLAGS+=-O3
sgi2yuv: configfile.h configfile.c global.h sgi2yuv.c Makefile
	gcc ${CFLAGS} configfile.c sgi2yuv.c -o sgi2yuv
clean:
	-rm sgi2yuv
