/*!
 ***********************************************************************
 * \file
 *    configfile.c
 * \brief
 *    Configuration handling.
 * \author
 *    Stephan Wenger           <stewe@cs.tu-berlin.de>
 *
 * Format is line oriented, maximum of one parameter per line
 *
 * Lines have the following format:
 * <ParameterName> = <ParameterValue> # Comments \\n
 * Whitespace is space and \\t
 * \par
 * <ParameterName> are the predefined names for Parameters and are case sensitive.
 *   See configfile.h for the definition of those names and their
 *   mapping to configinput->values.
 * \par
 * <ParameterValue> are either integers *[0..9]* or strings.
 *   Integers must fit into the wordlengths, signed values are
 *.  generally assumed. Strings containing no whitespace characters
 *   can be used directly. Strings containing whitespace characters
 *   are to be inclosed in double quotes ("string with whitespace").
 *   The double quote character is forbidden (may want to implement
 *   something smarter here).
 * \par
 *   Any Parameters whose ParameterName is undefined lead to the
 *   termination of the program with an error message.
 *
 * \par Known bug/Shortcoming:
 *    zero-length strings (i.e. to signal an non-existing file
 *    have to be coded as "".
 *
 * \par Rules for using command files
 *   All Parameters are initially taken from DEFAULTCONFIGFILENAME,
 *   defined in configfile.h. If an -f <config> parameter is present
 *   in the command line then this file is used to update the defaults
 *   of DEFAULTCONFIGFILENAME. There can be more than one -f parameters
 *   present. If -p <ParameterName = ParameterValue> parameters are
 *   present then these overide the default and the additional config
 *   file's settings, and are themselfes overridden by future -p
 *   parameters. There must be whitespace between -f and -p commands
 *   and their respecitive parameters.
 ***********************************************************************
 */

#define INCLUDED_BY_CONFIGFILE_C

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "configfile.h"
#include "global.h"

static char *GetConfigFileContent(char *Filename);
static void ParseContent(char *buf, int bufsize);
static int ParameterNameToMapIndex(char *s);

#define MAX_ITEMS_TO_PARSE 10000

/*!
 ***********************************************************************
 * \brief
 *    Parse the command line parameters and read the config files.
 * \param ac
 *    number of command line parameters
 * \param av
 *    command line parameters
 ***********************************************************************
 */
void Configure(int ac, char *av[]) {
  char *content;
  int CLcount, ContentLen, NumberParams;

  memset(&configinput, 0, sizeof(InputParameters));
  // Process default config file

  printf("Parsing Configfile %s", DEFAULTCONFIGFILENAME);
  content = GetConfigFileContent(DEFAULTCONFIGFILENAME);
  ParseContent(content, strlen(content));
  printf("\n");
  free(content);

  // Parse the command line

  CLcount = 1;
  while (CLcount < ac) {
    if (0 == strncmp(av[CLcount], "-f", 2)) // A file parameter?
    {
      content = GetConfigFileContent(av[CLcount + 1]);
      printf("Parsing Configfile %s", av[CLcount + 1]);
      ParseContent(content, strlen(content));
      printf("\n");
      free(content);
      CLcount += 2;
    } else {
      if (0 == strncmp(av[CLcount], "-p", 2)) // A config change?
      {
        // Collect all data until next parameter (starting with -<x> (x is any
        // character)), put it into content, and parse content.

        CLcount++;
        ContentLen = 0;
        NumberParams = CLcount;

        // determine the necessary size for content
        while (NumberParams < ac && av[NumberParams][0] != '-')
          ContentLen += strlen(av[NumberParams++]); // Space for all the strings
        ContentLen += 1000; // Additional 1000 bytes for spaces and \0s

        if ((content = malloc(ContentLen)) == NULL)
          no_mem_exit("Configure: content");
        ;
        content[0] = '\0';

        // concatenate all parameters itendified before

        while (CLcount < NumberParams) {
          char *source = &av[CLcount][0];
          char *destin = &content[strlen(content)];

          while (*source != '\0') {
            if (*source ==
                '=') // The Parser expects whitespace before and after '='
            {
              *destin++ = ' ';
              *destin++ = '=';
              *destin++ = ' '; // Hence make sure we add it
            } else
              *destin++ = *source;
            source++;
          }
          *destin = '\0';
          CLcount++;
        }
        printf("Parsing command line string '%s'", content);
        ParseContent(content, strlen(content));
        free(content);
        printf("\n");
      } else {
        snprintf(errortext, ET_SIZE,
                 "Error in command line, ac %d, around string '%s', missing -f "
                 "or -p parameters?",
                 CLcount, av[CLcount]);
        error(errortext, 300);
      }
    }
  }
  printf("\n");
}

/*!
 ***********************************************************************
 * \brief
 *    allocates memory buf, opens file Filename in f, reads contents into
 *    buf and returns buf
 * \param Filename
 *    name of config file
 ***********************************************************************
 */
char *GetConfigFileContent(char *Filename) {
  unsigned FileSize;
  FILE *f;
  char *buf;

  if (NULL == (f = fopen(Filename, "r"))) {
    snprintf(errortext, ET_SIZE, "Cannot open configuration file %s.\n",
             Filename);
    error(errortext, 300);
  }

  if (0 != fseek(f, 0, SEEK_END)) {
    snprintf(errortext, ET_SIZE, "Cannot fseek in configuration file %s.\n",
             Filename);
    error(errortext, 300);
  }

  FileSize = ftell(f);
  if (FileSize < 0 || FileSize > 60000) {
    snprintf(errortext, ET_SIZE,
             "Unreasonable Filesize %d reported by ftell for configuration "
             "file %s.\n",
             FileSize, Filename);
    error(errortext, 300);
  }
  if (0 != fseek(f, 0, SEEK_SET)) {
    snprintf(errortext, ET_SIZE, "Cannot fseek in configuration file %s.\n",
             Filename);
    error(errortext, 300);
  }

  if ((buf = malloc(FileSize + 1)) == NULL)
    no_mem_exit("GetConfigFileContent: buf");

  // Note that ftell() gives us the file size as the file system sees it.  The
  // actual file size, as reported by fread() below will be often smaller due to
  // CR/LF to CR conversion and/or control characters after the dos EOF marker
  // in the file.

  FileSize = fread(buf, 1, FileSize, f);
  buf[FileSize] = '\0';

  fclose(f);
  return buf;
}

/*!
 ***********************************************************************
 * \brief
 *    Parses the character array buf and writes global variable input,
 *    which is defined in configfile.h. This hack will continue to be
 *    necessary to facilitate the addition of new parameters through
 *    the Map[] mechanism (Need compiler-generated addresses in map[]).
 *
 *  \param buf
 *    buffer to be parsed
 *  \param bufsize
 *    buffer size of buffer
 ***********************************************************************
 */
void ParseContent(char *buf, int bufsize) {

  char *items[MAX_ITEMS_TO_PARSE];
  int MapIdx;
  int item = 0;
  int InString = 0, InItem = 0;
  char *p = buf;
  char *bufend = &buf[bufsize];
  int IntContent;
  int i;

  // Stage one: Generate an argc/argv-type list in items[], without comments and
  // whitespace. This is context insensitive and could be done most easily with
  // lex(1).

  while (p < bufend) {
    switch (*p) {
    case 13:
      p++;
      break;
    case '#':    // Found comment
      *p = '\0'; // Replace '#' with '\0' in case of comment immediately
                 // following integer or string
      while (*p != '\n' &&
             p < bufend) // Skip till EOL or EOF, whichever comes first
        p++;
      InString = 0;
      InItem = 0;
      break;
    case '\n':
      InItem = 0;
      InString = 0;
      *p++ = '\0';
      break;
    case ' ':
    case '\t': // Skip whitespace, leave state unchanged
      if (InString)
        p++;
      else { // Terminate non-strings once whitespace is found
        *p++ = '\0';
        InItem = 0;
      }
      break;

    case '"': // Begin/End of String
      *p++ = '\0';
      if (!InString) {
        items[item++] = p;
        InItem = ~InItem;
      } else
        InItem = 0;
      InString = ~InString; // Toggle
      break;

    default:
      if (!InItem) {
        items[item++] = p;
        InItem = ~InItem;
      }
      p++;
    }
  }

  item--;

  for (i = 0; i < item; i += 3) {
    if (0 > (MapIdx = ParameterNameToMapIndex(items[i]))) {
      snprintf(
          errortext, ET_SIZE,
          " Parsing error in config file: Parameter Name '%s' not recognized.",
          items[i]);
      error(errortext, 300);
    }
    if (strcmp("=", items[i + 1])) {
      snprintf(errortext, ET_SIZE,
               " Parsing error in config file: '=' expected as the second "
               "token in each line.");
      error(errortext, 300);
    }

    // Now interprete the Value, context sensitive...

    switch (Map[MapIdx].Type) {
    case 0: // Numerical
      if (1 != sscanf(items[i + 2], "%d", &IntContent)) {
        snprintf(errortext, ET_SIZE,
                 " Parsing error: Expected numerical value for Parameter of "
                 "%s, found '%s'.",
                 items[i], items[i + 2]);
        error(errortext, 300);
      }
      *(int *)(Map[MapIdx].Place) = IntContent;
      printf(".");
      break;
    case 1:
      strcpy((char *)Map[MapIdx].Place, items[i + 2]);
      printf(".");
      break;
    default:
      assert("Unknown value type in the map definition of configfile.h");
    }
  }
  memcpy(input, &configinput, sizeof(InputParameters));
}

/*!
 ***********************************************************************
 * \brief
 *    Returns the index number from Map[] for a given parameter name.
 * \param s
 *    parameter name string
 * \return
 *    the index number if the string is a valid parameter name,
 *    -1 for error
 ***********************************************************************
 */
static int ParameterNameToMapIndex(char *s) {
  int i = 0;

  while (Map[i].TokenName != NULL)
    if (0 == strcmp(Map[i].TokenName, s))
      return i;
    else
      i++;
  return -1;
};
