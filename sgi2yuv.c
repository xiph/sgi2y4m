
//------------------------------------------------------------
// COPYRIGHT AND WARRANTY INFORMATION
//
// This is free software so feel free to use it for whatever purpose
// you want to use it. If you extend or change this program it would
// be really nice to let me know (oelbaum@tum.de), so that more people
// can profit from your work.
//
// DISCLAIMER OF WARRANTY
//
// This software is provided on a "as it is" basis. The right use of this
// software is up to the user. In no event shall the
// contributor be liable for any incidental, punitive, or
// consequential damages of any kind whatsoever arising from the
// use of this program.
//------------------------------------------------------------

//------------------------------------------------------------
//
// File
//  sgi2yuv.c
// Brief
//  takes sgi16 files (RGB with 16 bit per component) and converts
//  that to YUV (currently only 8 bit supported)
//
// Author
//  Tobias Oelbaum  <oelbaum@tum.de>
//  Bugreports, comments, enhancement or speed-up suggestions are very welcome!
//
// Comments
//  Filter taps were provided by Gary Sullivan <garysull@microsoft.com>
//  configfile.c was copied from the AVC/H264 reference implementation
//
//
//  Version: 0.4
//------------------------------------------------------------
#include "configfile.h"
#include "global.h"

//------------------------------------------------------------
//
// Global variables
//
//------------------------------------------------------------

FILE *in, *out;

InputParameters inputs, *input = &inputs;
ImageParameters images, *img = &images;

char errortext[ET_SIZE]; //!< buffer for error message for exit with error()

int FILTER_LENGTH;

// RGB input image
double **imgR_rgb;
double **imgG_rgb;
double **imgB_rgb;

// internal YUV image
double **imgY;
double **imgPb;
double **imgPr;

double **imgPbSh;
double **imgPrSh;
double **imgPbSv;
double **imgPrSv;

// YUV output image
int **imgY_out;
int **imgU_out;
int **imgV_out;

void (*write_image_yuv)();
void (*write_image_abekas)();

#define NUMBER_MAX 24

//------------------------------------------------------------
//
//  Main routine
//
//------------------------------------------------------------
int main(int argc, char **argv) {

  int i, j;
  int file_no;
  int len;
  char number[NUMBER_MAX];
  int rgb_scale = 65535; // we have 16 bit inputs
  int ymax, ymin, yrange, yzero;
  int umax, umin, urange, uzero;

  // read config file
  Configure(argc, argv);

  // some initial settings
  img->width = input->width;
  img->height = input->height;
  img->frame_no = 0;
  FILTER_LENGTH = 8; // length of the subsampling filter

  if (input->yuv_format == 1 && input->subsampling != 1) {
    input->subsampling = 1;
    snprintf(errortext, ET_SIZE,
             "For Abekas files only 422 color subsampling supported");
  }

  switch (input->bpp_out) {
  case 8:
    ymax = 235;
    ymin = 16;
    yrange = 219;
    yzero = 16;
    umax = 240;
    umin = 16;
    urange = 224;
    uzero = 128;
    write_image_yuv = write_image_yuv8bit;
    write_image_abekas = write_image_abekas8bit;
    break;
  case 10:
    ymax = 943;
    ymin = 64;
    yrange = 879;
    yzero = 64;
    umax = 963;
    umin = 64;
    urange = 899;
    uzero = 512;
    if (input->stuffing) {
      write_image_yuv = write_image_yuv10bit_st;
      write_image_abekas = write_image_abekas10bit_st;
    } else {
      write_image_yuv = write_image_yuv10bit;
      write_image_abekas = write_image_abekas10bit;
    }
    break;
  default:
    snprintf(errortext, ET_SIZE, "Sorry, only 8, and 10 bit output supported");
    error(errortext, 1);
    break;
  }

  // allocate memory
  malloc_memory();

  if (!input->yuv_format) {
    strcat(input->outfile, ".yuv");
    if ((out = fopen(input->outfile, "wb")) == NULL) {
      snprintf(errortext, ET_SIZE, "Error open file %s  \n", input->outfile);
      error(errortext, 1);
    }
  }

  // for all frames according to the config file
  for (img->frame_no = 0; img->frame_no < input->no_frames; img->frame_no++) {
    // generate name of the input file
    file_no = input->infile_first + img->frame_no;
    snprintf(number, NUMBER_MAX, "%d", file_no);
    len = strlen(number);

    strncpy(img->infile, input->infile_pre, sizeof(input->infile_pre));
    for (i = 0; i < input->infile_count - len; i++)
      strcat(img->infile, "0");
    strcat(img->infile, number);
    strcat(img->infile, input->infile_suf);

    // open input file
    if ((in = fopen(img->infile, "rb")) == NULL) {
      snprintf(errortext, ET_SIZE, "Error open file %s  \n", img->infile);
      error(errortext, 1);
    }

    // read input frame
    read_frame();

    // scale input values and convert to YPbPr according to Rec 709
    for (j = 0; j < img->height; j++)  // al rows
      for (i = 0; i < img->width; i++) // al colums
      {
        // scale input values to a range from 0 to 1
        imgR_rgb[j][i] = (imgR_rgb[j][i]) / rgb_scale;
        imgG_rgb[j][i] = (imgG_rgb[j][i]) / rgb_scale;
        imgB_rgb[j][i] = (imgB_rgb[j][i]) / rgb_scale;

        if (img->width <= 720) {
          // convert according to Rec 601 conversion between R'G'B' and YPbPr
          imgY[j][i] = 0.299 * imgR_rgb[j][i] + 0.587 * imgG_rgb[j][i] +
                       0.114 * imgB_rgb[j][i];
          imgPb[j][i + FILTER_LENGTH] =
              (0.5 / 0.886) * (imgB_rgb[j][i] - imgY[j][i]);
          imgPr[j][i + FILTER_LENGTH] =
              (0.5 / 0.701) * (imgR_rgb[j][i] - imgY[j][i]);
        } else {
          // convert according to Rec 709 conversion between R'G'B' and YPbPr
          imgY[j][i] = 0.2126 * imgR_rgb[j][i] + 0.7152 * imgG_rgb[j][i] +
                       0.0722 * imgB_rgb[j][i];
          imgPb[j][i + FILTER_LENGTH] =
              (1 / 1.8556) * (imgB_rgb[j][i] - imgY[j][i]);
          imgPr[j][i + FILTER_LENGTH] =
              (1 / 1.5748) * (imgR_rgb[j][i] - imgY[j][i]);
        }
        // Now we hav Y in a range from 0 to 1
        // Pb Pr range from -0.5 to 0.5
      }

    // Color subsampling if necessary
    if (input->subsampling) {
      img->width_uv = img->width / 2;
      img->height_uv = img->height;
      // Horizontal subsampling to 422
      Hsubsampling(imgPb, imgPbSh, 2, 1);
      Hsubsampling(imgPr, imgPrSh, 2, 1);
      if (input->subsampling == 2) // output should be 4:2:0
      {
        img->height_uv = img->height / 2;
        // Vertical subsampling to 420
        Vsubsampling(imgPbSh, imgPbSv, 2, 1);
        Vsubsampling(imgPrSh, imgPrSv, 2, 1);
      }
    } else {
      img->width_uv = img->width;
      img->height_uv = img->height;
    }

    // create output image
    // up to now we had 16 bit inputs and all arithmetics have been made with
    // double precision conversion to 8 bits is done at the very end
    switch (input->subsampling) {
    case 0:                              // no subsampling
      for (j = 0; j < img->height; j++)  // al rows
        for (i = 0; i < img->width; i++) // al colums
        {
          imgY_out[j][i] = Value2Code(imgY[j][i], ymax, ymin, yrange, yzero);
          imgU_out[j][i] = Value2Code(imgPb[j][i + FILTER_LENGTH], umax, umin,
                                      urange, uzero);
          imgV_out[j][i] = Value2Code(imgPr[j][i + FILTER_LENGTH], umax, umin,
                                      urange, uzero);
        }
      break;
    case 1:                              // 422
      for (j = 0; j < img->height; j++)  // al rows
        for (i = 0; i < img->width; i++) // al colums
          imgY_out[j][i] = Value2Code(imgY[j][i], ymax, ymin, yrange, yzero);
      for (j = 0; j < img->height; j++)      // al rows
        for (i = 0; i < img->width / 2; i++) // al colums
        {
          imgU_out[j][i] = Value2Code(imgPbSh[j + FILTER_LENGTH][i], umax, umin,
                                      urange, uzero);
          imgV_out[j][i] = Value2Code(imgPrSh[j + FILTER_LENGTH][i], umax, umin,
                                      urange, uzero);
        }
      break;
    case 2:                              // 420
      for (j = 0; j < img->height; j++)  // al rows
        for (i = 0; i < img->width; i++) // al colums
          imgY_out[j][i] = Value2Code(imgY[j][i], ymax, ymin, yrange, yzero);
      for (j = 0; j < img->height / 2; j++)  // al rows
        for (i = 0; i < img->width / 2; i++) // al colums
        {
          imgU_out[j][i] = Value2Code(imgPbSv[j][i], umax, umin, urange, uzero);
          imgV_out[j][i] = Value2Code(imgPrSv[j][i], umax, umin, urange, uzero);
        }
      break;
    }

    // write file (yuv or abekas)
    if (!input->yuv_format)
      write_image_yuv();
    else {
      strncpy(img->outfile, input->outfile, sizeof(input->outfile));
      snprintf(number, NUMBER_MAX, "%d", img->frame_no);
      len = strlen(number);

      for (i = len; i < 5; i++)
        strcat(img->outfile, "0");
      strcat(img->outfile, number);
      strcat(img->outfile, ".qnt");
      if ((out = fopen(img->outfile, "wb")) == NULL) {
        snprintf(errortext, ET_SIZE, "Error open file %s  \n", img->outfile);
        error(errortext, 1);
      }
      write_image_abekas();
      fclose(out);
    }

    printf("Processed %s \n", img->infile);

  } // End all frames
  fclose(out);
  free_memory();
  return 0;
}

//------------------------------------------------------------
//
//  Horizontal 444 to 422 subsampling for PbPr
//  Note: we have a phase offset here!
//
//------------------------------------------------------------
void Hsubsampling(double **full, double **sub, int Hfactor, int filterh) {

  double tab[8];
  int i, j;
  int k = 0, l = 0;

  // initialize filter
  // 444 -> 422
  switch (filterh) // keep possibility to add different filter tabs
  {
  case 1:
    tab[0] = (double)0.50453345032298;
    tab[1] = (double)0.31577823859943;
    tab[2] = (double)0.0;
    tab[3] = (double)-0.09154810319329;
    tab[4] = (double)0.0;
    tab[5] = (double)0.04066666714886;
    tab[6] = (double)0.0;
    tab[7] = (double)-0.01716352771649;
    break;
  default:
    snprintf(errortext, ET_SIZE, "Filter not supported\n");
    error(errortext, 1);
    break;
  }

  // padd borders (mirror)
  // left border
  for (j = 0; j < img->height; j++)
    for (i = 0; i < FILTER_LENGTH; i++)
      full[j][i] = full[j][(FILTER_LENGTH * 2 - 1) - i + 1];

  // right border
  for (j = 0; j < img->height; j++) {
    k = 0;
    for (i = img->width + FILTER_LENGTH; i < img->width + FILTER_LENGTH * 2;
         i++) {
      k++;
      full[j][i] = full[j][img->width + FILTER_LENGTH - k - 1];
    }
  }

  k = 0;
  // perform filtering and subsampling
  for (j = 0; j < img->height; j++) {
    for (i = FILTER_LENGTH; i < img->width + FILTER_LENGTH; i += Hfactor) {
      sub[l + FILTER_LENGTH][k] =
          full[j][i] * tab[0] + full[j][i - 1] * tab[1] +
          full[j][i + 1] * tab[1] + full[j][i - 2] * tab[2] +
          full[j][i + 2] * tab[2] + full[j][i - 3] * tab[3] +
          full[j][i + 3] * tab[3] + full[j][i - 4] * tab[4] +
          full[j][i + 4] * tab[4] + full[j][i - 5] * tab[5] +
          full[j][i + 5] * tab[5] + full[j][i - 6] * tab[6] +
          full[j][i + 6] * tab[6] + full[j][i - 7] * tab[7] +
          full[j][i + 7] * tab[7];
      k++;
    }
    k = 0;
    l++;
  }
}

//------------------------------------------------------------
//
//  Vertical 422 to 420 subsampling for PbPr
//  Note: position of the chroma values is the same way it is for
//  MPEG-2, MPEG-4 Part 2, MPEG-4 Part 10 (aka AVC), H.263, H.264
//
//------------------------------------------------------------
void Vsubsampling(double **full, double **sub, int Vfactor, int filterv) {

  double tab[8];
  int i, j;
  int k = 0, l = 0;

  // initialize filter
  // 422 -> 420
  switch (filterv) // keep possibility to add different filter tabs
  {
  case 1:
    tab[0] = (double)0.45417830962846;
    tab[1] = (double)0.14630826357715;
    tab[2] = (double)-0.08189331229717;
    tab[3] = (double)-0.05254456550808;
    tab[4] = (double)0.03519540819902;
    tab[5] = (double)0.02360533018213;
    tab[6] = (double)-0.01539537217249;
    tab[7] = (double)-0.00945406160902;
    break;
  default:
    snprintf(errortext, ET_SIZE, "Filter not supported\n");
    error(errortext, 1);
    break;
  }

  // padd borders (mirror)
  // top border
  for (j = 0; j < FILTER_LENGTH; j++)
    for (i = 0; i < img->width / 2; i++)
      full[j][i] = full[(FILTER_LENGTH * 2 - 1) - j + 1][i];

  // botton border
  k = 0;
  for (j = img->height + FILTER_LENGTH; j < img->height + FILTER_LENGTH * 2;
       j++) {
    k++;
    for (i = 0; i < img->width / 2; i++) {
      full[j][i] = full[img->height + FILTER_LENGTH - k - 1][i];
      full[j][i] = full[img->height + FILTER_LENGTH - k - 1][i];
    }
  }

  k = 0;
  // perform filtering and subsampling
  for (j = FILTER_LENGTH; j < img->height + FILTER_LENGTH; j += Vfactor) {
    for (i = 0; i < img->width / 2; i++) {
      sub[l][k] = full[j][i] * tab[0] + full[j + 1][i] * tab[0] +
                  full[j - 1][i] * tab[1] + full[j + 2][i] * tab[1] +
                  full[j - 2][i] * tab[2] + full[j + 3][i] * tab[2] +
                  full[j - 3][i] * tab[3] + full[j + 4][i] * tab[3] +
                  full[j - 4][i] * tab[4] + full[j + 5][i] * tab[4] +
                  full[j - 5][i] * tab[5] + full[j + 6][i] * tab[5] +
                  full[j - 6][i] * tab[6] + full[j + 7][i] * tab[6] +
                  full[j - 7][i] * tab[7] + full[j + 8][i] * tab[7];
      k++;
    }
    k = 0;
    l++;
  }
}

//------------------------------------------------------------
//
//  Assign an 8 bit integer code to a certain yuv value
//  according to headroom and footroom, clip if higher
//
//------------------------------------------------------------
int Value2Code(double f, int max, int min, int range, int zero) {
  double code;
  code = f * range + zero;
  return (unsigned int)((code < min)
                            ? min
                            : ((code < max - 0.5) ? (code + 0.5) : max));
}

//------------------------------------------------------------
//
//  write yuv image planar yuv 8 bit
//
//------------------------------------------------------------
void write_image_yuv8bit() {
  unsigned char *buf;
  int i, j;

  buf = (unsigned char *)malloc(img->width * sizeof(*buf));
  for (j = 0; j < img->height; j++) {
    for (i = 0; i < img->width; i++)
      buf[i] = (unsigned char)imgY_out[j][i];
    fwrite(buf, img->width, sizeof(*buf), out);
  }

  for (j = 0; j < img->height_uv; j++) {
    for (i = 0; i < img->width_uv; i++)
      buf[i] = (unsigned char)imgU_out[j][i];
    fwrite(buf, img->width_uv, sizeof(*buf), out);
  }

  for (j = 0; j < img->height_uv; j++) {
    for (i = 0; i < img->width_uv; i++)
      buf[i] = (unsigned char)imgV_out[j][i];
    fwrite(buf, img->width_uv, sizeof(*buf), out);
  }

  free(buf);
  fflush(out);
}

//------------------------------------------------------------
//
//  write yuv image planar yuv 10 bit
//
//------------------------------------------------------------
void write_image_yuv10bit() {
  int stride = (img->width + 3) >> 2;
  int stride_uv = (img->width_uv + 3) >> 2;
  unsigned char *buf;
  int i, j;

  buf = (unsigned char *)malloc(5 * stride * sizeof(*buf));
  for (j = 0; j < img->height; j++) {
    for (i = 0; i < img->width; i += 4) {
      buf[5 * (i >> 2) + 0] = imgY_out[j][i] >> 2;
      buf[5 * (i >> 2) + 1] =
          ((imgY_out[j][i] & 3) << 6) + (imgY_out[j][i + 1] >> 4);
      buf[5 * (i >> 2) + 2] =
          ((imgY_out[j][i + 1] & 15) << 4) + (imgY_out[j][i + 2] >> 6);
      buf[5 * (i >> 2) + 3] =
          ((imgY_out[j][i + 2] & 63) << 2) + (imgY_out[j][i + 3] >> 8);
      buf[5 * (i >> 2) + 4] = (imgY_out[j][i + 3] & 255);
    }
    fwrite(buf, 5 * stride, sizeof(*buf), out);
  }

  for (j = 0; j < img->height_uv; j++) {
    for (i = 0; i < img->width_uv; i += 4) {
      buf[5 * (i >> 2) + 0] = imgU_out[j][i] >> 2;
      buf[5 * (i >> 2) + 1] =
          ((imgU_out[j][i] & 3) << 6) + (imgU_out[j][i + 1] >> 4);
      buf[5 * (i >> 2) + 2] =
          ((imgU_out[j][i + 1] & 15) << 4) + (imgU_out[j][i + 2] >> 6);
      buf[5 * (i >> 2) + 3] =
          ((imgU_out[j][i + 2] & 63) << 2) + (imgU_out[j][i + 3] >> 8);
      buf[5 * (i >> 2) + 4] = (imgU_out[j][i + 3] & 255);
    }
    fwrite(buf, 5 * stride_uv, sizeof(*buf), out);
  }

  for (j = 0; j < img->height_uv; j++) {
    for (i = 0; i < img->width_uv; i += 4) {
      buf[5 * (i >> 2) + 0] = imgV_out[j][i] >> 2;
      buf[5 * (i >> 2) + 1] =
          ((imgV_out[j][i] & 3) << 6) + (imgV_out[j][i + 1] >> 4);
      buf[5 * (i >> 2) + 2] =
          ((imgV_out[j][i + 1] & 15) << 4) + (imgV_out[j][i + 2] >> 6);
      buf[5 * (i >> 2) + 3] =
          ((imgV_out[j][i + 2] & 63) << 2) + (imgV_out[j][i + 3] >> 8);
      buf[5 * (i >> 2) + 4] = (imgV_out[j][i + 3] & 255);
    }
    fwrite(buf, 5 * stride_uv, sizeof(*buf), out);
  }
  free(buf);
}

//------------------------------------------------------------
//
//  write yuv image planar yuv 10 bit stuffing to 16 bit words
//
//------------------------------------------------------------
void write_image_yuv10bit_st() {
  unsigned char *buf;
  int i, j;

  buf = (unsigned char *)malloc(2 * img->width * sizeof(*buf));
  for (j = 0; j < img->height; j++) {
    for (i = 0; i < img->width; i++) {
      buf[2 * i + 0] = imgY_out[j][i] >> 2;
      buf[2 * i + 1] = ((imgY_out[j][i] & 3) << 6);
    }
    fwrite(buf, 2 * img->width, sizeof(*buf), out);
  }

  for (j = 0; j < img->height_uv; j++) {
    for (i = 0; i < img->width_uv; i++) {
      buf[2 * i + 0] = imgU_out[j][i] >> 2;
      buf[2 * i + 1] = ((imgU_out[j][i] & 3) << 6);
    }
    fwrite(buf, 2 * img->width, sizeof(*buf), out);
  }

  for (j = 0; j < img->height_uv; j++) {
    for (i = 0; i < img->width_uv; i++) {
      buf[2 * i + 0] = imgV_out[j][i] >> 2;
      buf[2 * i + 1] = ((imgV_out[j][i] & 3) << 6);
    }
    fwrite(buf, 2 * img->width, sizeof(*buf), out);
  }
  free(buf);
}

//------------------------------------------------------------
//
//  write yuv image abekas format 8 bit
//
//------------------------------------------------------------
void write_image_abekas8bit() {
  int stride = (img->width + 1) >> 1;
  unsigned char *buf;
  int i, j;

  buf = (unsigned char *)malloc(4 * stride * sizeof(*buf));
  for (j = 0; j < img->height; j++) {
    for (i = 0; i < img->width; i += 2) {
      buf[2 * i + 0] = (unsigned char)(imgU_out[j][i / 2]);
      buf[2 * i + 1] = (unsigned char)(imgY_out[j][i]);
      buf[2 * i + 2] = (unsigned char)(imgV_out[j][i / 2]);
      buf[2 * i + 3] = (unsigned char)(imgY_out[j][i + 1]);
    }
    fwrite(buf, 4 * stride, sizeof(*buf), out);
  }

  free(buf);
  fflush(out);
}

//------------------------------------------------------------
//
//  write yuv image abekas format 10 bit
//
//------------------------------------------------------------
void write_image_abekas10bit() {
  int stride = (img->width + 1) >> 1;
  unsigned char *buf;
  int i, j;

  buf = (unsigned char *)malloc(5 * stride * sizeof(*buf));
  for (j = 0; j < img->height; j++) {
    for (i = 0; i < img->width; i += 2) {
      buf[5 * (i >> 1) + 0] = (imgU_out[j][i / 2] >> 2);
      buf[5 * (i >> 1) + 1] =
          ((imgU_out[j][i / 2] & 3) << 6) + (imgY_out[j][i] >> 4);
      buf[5 * (i >> 1) + 2] =
          ((imgY_out[j][i] & 15) << 4) + (imgV_out[j][i / 2] >> 6);
      buf[5 * (i >> 1) + 3] =
          ((imgV_out[j][i / 2] & 63) << 2) + (imgY_out[j][i + 1] >> 8);
      buf[5 * (i >> 1) + 4] = (imgY_out[j][i + 1] & 255);
    }
    fwrite(buf, 5 * stride, sizeof(*buf), out);
  }
  free(buf);
}

//------------------------------------------------------------
//
//  write yuv image abekas yuv 10 bit stuffing to 16 bit words
//
//------------------------------------------------------------
void write_image_abekas10bit_st() {
  int stride = (img->width + 1) >> 1;
  unsigned char *buf;
  int i, j;

  buf = (unsigned char *)malloc(8 * stride * sizeof(*buf));
  for (j = 0; j < img->height; j++) {
    for (i = 0; i < img->width; i += 2) {
      buf[4 * (i >> 1) + 0] = imgU_out[j][i / 2] >> 2;
      buf[4 * (i >> 1) + 1] = ((imgU_out[j][i / 2] & 3) << 6);

      buf[4 * (i >> 1) + 2] = imgY_out[j][i] >> 2;
      buf[4 * (i >> 1) + 3] = ((imgY_out[j][i] & 3) << 6);

      buf[4 * (i >> 1) + 4] = imgV_out[j][i / 2] >> 2;
      buf[4 * (i >> 1) + 5] = ((imgV_out[j][i / 2] & 3) << 6);

      buf[4 * (i >> 1) + 6] = imgY_out[j][i + 1] >> 2;
      buf[4 * (i >> 1) + 7] = ((imgY_out[j][i + 1] & 3) << 6);
    }
    fwrite(buf, 8 * stride, sizeof(*buf), out);
  }
  free(buf);
}

//------------------------------------------------------------
//
//  read input data
//
//------------------------------------------------------------
void read_frame() {
  int status;
  int i, j;

  long filesize;
  unsigned char *buf;

  filesize = img->height * img->width * 6;
  status = fseek(in, -filesize, SEEK_END);

  if (status != 0) {
    snprintf(errortext, ET_SIZE, "Error in seeking frame: %s\n", img->infile);
    error(errortext, 1);
  }

  // interleaved RGB //
  buf = (unsigned char *)malloc(2 * img->width * sizeof(*buf));
  for (j = 0; j < img->height; j++) {
    fread(buf, 2 * img->width, sizeof(*buf), in);
    for (i = 0; i < img->width; i++)
      imgR_rgb[img->height - 1 - j][i] = (buf[2 * i] << 8) + buf[2 * i + 1];
  }

  for (j = 0; j < img->height; j++) {
    fread(buf, 2 * img->width, sizeof(*buf), in);
    for (i = 0; i < img->width; i++)
      imgG_rgb[img->height - 1 - j][i] = (buf[2 * i] << 8) + buf[2 * i + 1];
  }

  for (j = 0; j < img->height; j++) {
    fread(buf, 2 * img->width, sizeof(*buf), in);
    for (i = 0; i < img->width; i++)
      imgB_rgb[img->height - 1 - j][i] = (buf[2 * i] << 8) + buf[2 * i + 1];
  }

  free(buf);
  fclose(in);
}

//------------------------------------------------------------
//
//  allocate memory
//
//------------------------------------------------------------
void malloc_memory() {
  int memory_size = 0;

  // allocate memory for input image
  memory_size += get_mem2D_double(&imgR_rgb, img->height, img->width);
  memory_size += get_mem2D_double(&imgG_rgb, img->height, img->width);
  memory_size += get_mem2D_double(&imgB_rgb, img->height, img->width);

  // allocate memory for internal yuv image
  memory_size += get_mem2D_double(&imgY, img->height, img->width);
  memory_size +=
      get_mem2D_double(&imgPb, img->height, img->width + FILTER_LENGTH * 2);
  memory_size +=
      get_mem2D_double(&imgPr, img->height, img->width + FILTER_LENGTH * 2);

  if (input->subsampling) {
    memory_size += get_mem2D_double(&imgPbSh, img->height + FILTER_LENGTH * 2,
                                    img->width / 2);
    memory_size += get_mem2D_double(&imgPrSh, img->height + FILTER_LENGTH * 2,
                                    img->width / 2);
  }

  if (input->subsampling == 2) {
    memory_size += get_mem2D_double(&imgPbSv, img->height / 2, img->width / 2);
    memory_size += get_mem2D_double(&imgPrSv, img->height / 2, img->width / 2);
  }

  // allocate memory for output image
  memory_size += get_mem2D(&imgY_out, img->height, img->width);
  memory_size += get_mem2D(&imgU_out, img->height, img->width);
  memory_size += get_mem2D(&imgV_out, img->height, img->width);
}

//------------------------------------------------------------
//
//  allocate memory 2D
//
//------------------------------------------------------------
int get_mem2D(int ***array2D, int rows, int columns) {
  int i;

  if ((*array2D = (int **)calloc(rows, sizeof(int *))) == NULL)
    no_mem_exit("get_mem2D: array2D");
  if (((*array2D)[0] = (int *)calloc(columns * rows, sizeof(int))) == NULL)
    no_mem_exit("get_mem2D: array2D");

  for (i = 1; i < rows; i++)
    (*array2D)[i] = (*array2D)[i - 1] + columns;

  return rows * columns;
}

//------------------------------------------------------------
//
//  allocate memory2D double
//
//------------------------------------------------------------
int get_mem2D_double(double ***array2D, int rows, int columns) {
  int i;

  if ((*array2D = (double **)calloc(rows, sizeof(double *))) == NULL)
    no_mem_exit("get_mem2D: array2D");
  if (((*array2D)[0] = (double *)calloc(columns * rows, sizeof(double))) ==
      NULL)
    no_mem_exit("get_mem2D: array2D");

  for (i = 1; i < rows; i++)
    (*array2D)[i] = (*array2D)[i - 1] + columns;

  return rows * columns;
}

//------------------------------------------------------------
//
//  exit if memory allocation failed
//
//------------------------------------------------------------
void no_mem_exit(char *where) {
  snprintf(errortext, ET_SIZE, "Could not allocate memory: %s", where);
  error(errortext, 100);
}

//------------------------------------------------------------
//
//  free memory
//
//------------------------------------------------------------
void free_memory() {

  free_mem2D_double(imgR_rgb);
  free_mem2D_double(imgG_rgb);
  free_mem2D_double(imgB_rgb);

  free_mem2D_double(imgY);
  free_mem2D_double(imgPb);
  free_mem2D_double(imgPr);

  free_mem2D(imgY_out);
  free_mem2D(imgU_out);
  free_mem2D(imgV_out);

  if (input->subsampling) {
    free_mem2D_double(imgPbSh);
    free_mem2D_double(imgPrSh);
  }

  if (input->subsampling == 2) {
    free_mem2D_double(imgPbSv);
    free_mem2D_double(imgPrSv);
  }
}

//------------------------------------------------------------
//
//  free memory2D
//
//------------------------------------------------------------
void free_mem2D(int **array2D) {
  if (array2D) {
    if (array2D[0])
      free(array2D[0]);
    else
      error("free_mem2D: trying to free unused memory", 100);

    free(array2D);
  } else {
    error("free_mem2D: trying to free unused memory", 100);
  }
}

//------------------------------------------------------------
//
//  free memory2D double
//
//------------------------------------------------------------
void free_mem2D_double(double **array2D) {
  if (array2D) {
    if (array2D[0])
      free(array2D[0]);
    else
      error("free_mem2D: trying to free unused memory", 100);

    free(array2D);
  } else {
    error("free_mem2D: trying to free unused memory", 100);
  }
}

//------------------------------------------------------------
//
//  error handling
//
//------------------------------------------------------------
void error(char *text, int code) {
  fprintf(stderr, "%s\n", text);
  exit(code);
}
